package actions;

import controls.ButtonControl;
import controls.TextfieldControl;
import utils.TestConfig;
import utils.TestConfiguration;
import webdriver.Driver;

public class ApplicationActions {

    public void gotoHomeSite() {
        TestConfig config = TestConfiguration.instance().getTestConfiguration();
        String url = config.getApplication().getUrl();
        Driver.getDriver().get(url);
    }

    public void login() {
        TestConfig config = TestConfiguration.instance().getTestConfiguration();
        String username = config.getApplication().getUsername();
        String password = config.getApplication().getPassword();
        ButtonActions.click("GİRİŞ Yap");
        TextfieldControl.fromLabel("Kullanıcı Adı").setText(username);
        TextfieldControl.fromLabel("Şifre").setText(password);
        ButtonControl.fromLabel("Giriş Yap").click();

    }
}
